#include <iostream>
#include "hashing.h"
#include <string>

using namespace std;

int main() {
    HashTable h;

    h.add(1, "world");
    h.add(2, "bye");


    cout << h.get(1) << endl;
    cout << h.get(2) << endl;

    h.update(2, "good bye");

    cout << h.get(2) << endl;

    h.add(1, "bababa");
    h.add(1, "aaaaaa");
    h.add(1, "babbbb");

    h.view(1);
    cout << endl;
    cout << "get 1" << h.get(1) << endl;

    h.remove(1);
    cout << "get 1" << h.get(1) << endl;



    return 0;
}