//
// Created by vicky131102 on 02/04/2020.
//
// hashtable.h
// An implementation of a hash table
#ifndef HASHING_HASHING_H
#define HASHING_HASHING_H

#include <iostream>
#include <list>
#include <string>

using namespace std;

class HashTable {
    private:
        struct Pair {
            int key;
            string value;
        };
        list<Pair> container[20];
        int getHash(int key);
    public:
        HashTable();
        void add(int key, string value);
        string get(int key);
        void view(int key);
        bool update(int key, string value);
        bool remove(int key);
};

HashTable::HashTable() {
    // Initialize the linkedlists
    // We have to do this because when we define an array,
    // the values are nonexistent and cannot be compared.
    for (int i = 0; i < 20; i++) {
        container[i] = list<Pair>();
    }
}

int HashTable::getHash(int key) {
    // Hash function
    // For this example, we will use modulo-division method
    // as our hash function.
    return key % (20);
    //
}

void HashTable::add(int key, string value) {
    Pair pair;
    pair.key = key;
    pair.value = value;
    int hash = getHash(key);
    container[hash].push_back(pair);
}

string HashTable::get(int key) {
    int hash = getHash(key);
    list<Pair> *currentLL = &container[hash];

    for (list<Pair>::iterator itr = currentLL->begin(); itr != currentLL->end(); itr++) {
        if (itr->key == key) {
            return itr->value;
        }
    }

    return "";
}

void HashTable::view(int index) {
    list<Pair> *currentLL = &container[index];

    for (list<Pair>::iterator itr = currentLL->begin(); itr != currentLL->end(); itr++) {
        cout << itr->key << ", " << itr->value << "--> ";
    }

}

bool HashTable::update(int key, string value) {
    int hash = getHash(key);
    list<Pair> *currentLL = &container[hash];

    for (list<Pair>::iterator itr = currentLL->begin(); itr != currentLL->end(); itr++) {
        if (itr->key == key) {
            itr->value = value;
            return true;
        }
    }

    return false;
}

bool HashTable::remove(int key) {
    int hash = getHash(key);
    list<Pair> *currentLL = &container[hash];

    for (list<Pair>::iterator itr = currentLL->begin(); itr != currentLL->end(); itr++) {
        if (itr->key == key) {
            currentLL->erase(itr);
            return true;
        }
    }
    return false;
}

#endif //HASHING_HASHING_H
