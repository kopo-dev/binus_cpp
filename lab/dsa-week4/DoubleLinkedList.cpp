#include <iostream>
#include "DoubleLinkedList.hpp"

template <typename T>
DoubleLinkedList<T>::DoubleLinkedList(T data)
{
    if (data)
    {
        this->root = new Node<T>;
        this->root->data = data;
        this->root->prev = nullptr;
        this->root->next = nullptr;
        this->nb_node++;
    }
}

template <typename T>
void DoubleLinkedList<T>::insert(T data)
{
    Node<T> *current = this->root;
    Node<T> *new_node = new Node<T>;

    new_node->data = data;
    new_node->next = nullptr;

    if (current == nullptr)
    {
        this->root = new_node;
        this->root->next = nullptr;
        this->root->prev = nullptr;
    }
    else
    {
        while (current->next != nullptr)
            current = current->next;
        new_node->prev = current;
        current->next = new_node;
    }
    this->nb_node++;
}

template <typename T>
void DoubleLinkedList<T>::remove(int index)
{
    Node<T> *current = this->root;
    Node<T> *previous = nullptr;
    int i = 0;
    while (current != nullptr)
    {
        if (i == index)
        {
            if (current == this->root || previous == nullptr)
                this->root = current->next;
            else {
                previous->next = current->next;
                // set prev 
            }
            delete (current);
            this->nb_node--;
            return;
        }
        previous = current;
        current = current->next;
        i++;
    }
    std::cout << "Nothing to delete at index " << index << std::endl;
}

template <typename T>
T DoubleLinkedList<T>::get(int index)
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        if (i == index)
        {
            return current->data;
        }
        current = current->next;
        i++;
    }
}

template <typename T>
int DoubleLinkedList<T>::size()
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        i++;
    }
    return i;
}

template <typename T>
void DoubleLinkedList<T>::print()
{
    Node<T> *current = this->root;

    if (this->nb_node <= 1)
        std::cout << "The list is empty" << std::endl;
    else
    {
        while (current != nullptr)
        {
            std::cout << current->data << " ";
            current = current->next;
        }
    }
    std::cout << "Nb of elem: " << this->nb_node << std::endl;
}