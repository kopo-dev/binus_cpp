#include <iostream>

#include "DoubleLinkedList.hpp"
#include "DoubleLinkedList.cpp"

int main()
{
    std::cout << "Testing DSA-Week4" << std::endl;

    std::cout << "==============================" << std::endl;
    std::cout << "===== Double Linked List =====" << std::endl;
    std::cout << "==============================" << std::endl << std::endl;

    DoubleLinkedList<const char *> list("J'aime");
    list.remove(0);
    list.remove(2);
    list.remove(20);
    list.print();
    std::cout << "List size: " << list.size() << std::endl;
    list.insert("J'aime");
    list.insert("les");
    list.insert("chips");
    list.insert("au");
    list.insert("fromage");
    std::cout << "List size: " << list.size() << std::endl;

    return 0;
}