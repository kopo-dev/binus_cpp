#pragma once

#include "Node.hpp"
#include "MyIterator.hpp"

template <class T>
class DoubleLinkedList
{
private:
    Node<T> *root = nullptr;
    int nb_node = 0;

public:
    DoubleLinkedList(T data = nullptr);
    void insert(T data);
    void remove (int index);
    T get(int index);
    int size();
    void print(); 
};