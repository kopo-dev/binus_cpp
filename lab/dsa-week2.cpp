#include <iostream>

using namespace std;

bool isPalindrome(string str) {
    int i = 0;
    int end = str.length() - 1;

    while((i < str.length() && end > 0) && str[i] == str[end]) {
        i++;
        end--;
    }
    cout << i << " " << end << endl;
    if (i == str.length() - 1) {
        cout << "TRUE" << endl;
        return true;
    }
    cout << "FALSE" << endl;
    return false;
}

template <typename T>
void swap(T *a, T *b) {
    T *c;

    c = b;
    *b = *a;
    *a = *c;
}

int main() {
    //swap test
    int a = 6;
    int b = 9;

    cout << a << " " << b << endl;
    swap(a, b);
    cout << a << " " << b << endl;

    //palindrome test
    string c = "kayak";
    string d = "abricot";
    cout << "Is " << c << " a palindrome ?" << endl;
    isPalindrome(c);
    cout << "Is " << d << " a palindrome ?" << endl;
    isPalindrome(d);
    return 0;
}