#include <iostream>
#include "Stack.hpp"

template <typename T>
Stack<T>::Stack(T data)
{
    this->root = new Node<T>;
    this->root->data = data;
    this->root->next = nullptr;
    this->nb_node++;
}

// add to head, pop the head, head is first element
template <typename T>
void Stack<T>::push(T data)
{
    Node<T> *current = this->root;
    Node<T> *new_node = new Node<T>;

    new_node->data = data;
    new_node->next = this->root;

    this->root = new_node;
    this->nb_node++;
}

template <typename T>
T Stack<T>::pop()
{
    assert((this->root != nullptr) && "Cannot pop an empty stack");

    T node_data = this->root->data;
    Node<T> *new_root = this->root->next;

    delete(this->root);
    this->nb_node--;
    this->root = this->root->next;
    return node_data;
}

template <typename T>
int Stack<T>::size()
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        i++;
    }
    return i;
}

template <typename T>
void Stack<T>::print()
{
    Node<T> *current = this->root;

    while (current != nullptr)
    {
        std::cout << current->data << " ";
        current = current->next;
    }
    std::cout << std::endl
              << "Nb of elem: " << this->nb_node << std::endl;
}

template <typename T>
MyForwardIterator<T> Stack<T>::begin() const
{
    MyForwardIterator<T> itr(this->root);
    return itr;
}

template <typename T>
MyForwardIterator<T> Stack<T>::end() const
{
    Node<T> *current = this->root;
    while (current != nullptr)
    {
        current = current->next;
    }
    MyForwardIterator<T> itr(current);
    return itr;
}