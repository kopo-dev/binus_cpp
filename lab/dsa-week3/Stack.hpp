#pragma once

#include "Node.hpp"
#include "MyIterator.hpp"

template <class T>
class Stack
{
private:
    Node<T> *root = nullptr;
    int nb_node = 0;

public:
    Stack(T data);
    void push(T data);
    T pop ();
    int size();
    void print();
    // iterator
    MyForwardIterator<T> begin() const;
    MyForwardIterator<T> end() const; 
};