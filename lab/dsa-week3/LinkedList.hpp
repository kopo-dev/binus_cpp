#pragma once

#include "Node.hpp"
#include "MyIterator.hpp"

template <class T>
class LinkedList
{
private:
    Node<T> *root = nullptr;
    int nb_node = 0;

public:
    LinkedList(T data = nullptr);
    void insert(T data);
    void remove (int index);
    T get(int index);
    int size();
    void print();
    // iterator
    MyForwardIterator<T> begin() const;
    MyForwardIterator<T> end() const; 
};