#include <iostream>
#include "LinkedList.hpp"

template <typename T>
LinkedList<T>::LinkedList(T data)
{
    if (data)
    {
        this->root = new Node<T>;
        this->root->data = data;
        this->nb_node++;
    }
}

template <typename T>
void LinkedList<T>::insert(T data)
{
    Node<T> *current = this->root;
    Node<T> *new_node = new Node<T>;

    new_node->data = data;
    new_node->next = nullptr;

    if (current == nullptr)
    {
        this->root = new_node;
        this->root->next = nullptr;
    }
    else
    {
        while (current->next != nullptr)
            current = current->next;
        current->next = new_node;
    }
    this->nb_node++;
}

template <typename T>
void LinkedList<T>::remove(int index)
{
    Node<T> *current = this->root;
    Node<T> *previous = nullptr;
    int i = 0;
    while (current != nullptr)
    {
        if (i == index)
        {
            if (current == this->root || previous == nullptr)
                this->root = current->next;
            else
                previous->next = current->next;
            delete (current);
            this->nb_node--;
            return;
        }
        previous = current;
        current = current->next;
        i++;
    }
    std::cout << "Nothing to delete at index " << index << std::endl;
}

template <typename T>
T LinkedList<T>::get(int index)
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        if (i == index)
        {
            return current->data;
        }
        current = current->next;
        i++;
    }
}

template <typename T>
int LinkedList<T>::size()
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        i++;
    }
    return i;
}

template <typename T>
void LinkedList<T>::print()
{
    Node<T> *current = this->root;

    if (this->nb_node <= 1)
        std::cout << "The list is empty" << std::endl;
    else
    {
        while (current != nullptr)
        {
            std::cout << current->data << " ";
            current = current->next;
        }
    }
    std::cout << "Nb of elem: " << this->nb_node << std::endl;
}

template <typename T>
MyForwardIterator<T> LinkedList<T>::begin() const
{
    MyForwardIterator<T> itr(this->root);
    return itr;
}

template <typename T>
MyForwardIterator<T> LinkedList<T>::end() const
{
    Node<T> *current = this->root;
    while (current != nullptr)
    {
        current = current->next;
    }
    MyForwardIterator<T> itr(current);
    return itr;
}