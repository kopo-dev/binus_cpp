#pragma once

#include "Node.hpp"
#include "MyIterator.hpp"

template <class T>
class Queue
{
private:
    Node<T> *root = nullptr;
    int nb_node = 0;

public:
    Queue(T data);
    void push(T data);
    T pop();
    int size();
    void print();
    // iterator
    MyForwardIterator<T> begin() const;
    MyForwardIterator<T> end() const; 
};