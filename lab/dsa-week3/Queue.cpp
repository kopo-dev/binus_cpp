#include <iostream>
#include "Queue.hpp"

template <typename T>
Queue<T>::Queue(T data)
{
        this->root = new Node<T>;
        this->root->data = data;
        this->root->next = nullptr;
        this->nb_node++;
}

// add to head, pop the head, head is first element
template <typename T>
void Queue<T>::push(T data)
{
    Node<T> *current = this->root;
    Node<T> *new_node = new Node<T>;

    new_node->data = data;
    new_node->next = nullptr;

    if(this->root == nullptr) {
        this->root = new_node;
    }
    else {
        while (current->next != nullptr) {
            current = current->next;
        }
        current->next = new_node;
    }
    this->nb_node++;
}

template <typename T>
T Queue<T>::pop()
{
    assert((this->root != nullptr) && "Cannot pop an empty queue");
    T node_data = this->root->data;
    Node<T> *tmp = this->root;
    this->root = this->root->next;
    delete(tmp);
    this->nb_node--;
    return node_data; 
}

template <typename T>
int Queue<T>::size()
{
    Node<T> *current = this->root;
    int i = 0;
    while (current != nullptr)
    {
        current = current->next;
        i++;
    }
    return i;
}

template <typename T>
void Queue<T>::print()
{
    Node<T> *current = this->root;

    while (current != nullptr)
    {
        std::cout << current->data << " ";
        current = current->next;
    }
    std::cout << std::endl
              << "Nb of elem: " << this->nb_node << std::endl;
}

template <typename T>
MyForwardIterator<T> Queue<T>::begin() const
{ 
    MyForwardIterator<T> itr(this->root);
    return itr;
}

template <typename T>
MyForwardIterator<T> Queue<T>::end() const
{
    Node<T> *current = this->root;
    while (current != nullptr)
    {
        current = current->next;
    }
    MyForwardIterator<T> itr(current);
    return itr;
}