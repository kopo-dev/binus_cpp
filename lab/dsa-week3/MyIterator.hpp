#pragma once

#include <iostream>
#include <iterator>
#include <cstddef>      // ptrdiff_t
#include <type_traits>  // remove_cv_t
#include <cassert>      // assert
#include <utility>      //swap

#include "Node.hpp"

template
<
    class T,
    class UnqualifiedType = std::remove_cv_t<T>
>
class MyForwardIterator 
: public std::iterator<std::forward_iterator_tag,
                        UnqualifiedType,
                        std::ptrdiff_t,
                        T*,
                        T&>
{
    private:
        Node<UnqualifiedType> *iterator;
        
    public:
        explicit MyForwardIterator(Node<UnqualifiedType> *node) : iterator(node) {}
        MyForwardIterator() : iterator(nullptr) {}

        void swap(MyForwardIterator &other) noexcept {
            using std::swap;
            swap(iterator, other.iterator);
        }

        MyForwardIterator & operator++() // pre-increment
        {
            assert(iterator != nullptr && "Out of bounds interator increment");
            iterator = iterator->next;
            return *this;
        }

        MyForwardIterator operator++(int) // post-increment
        {
            assert(iterator != nullptr && "Out of bounds interator increment");
            MyForwardIterator tmp(*this);
            iterator = iterator->next;
            return tmp;
        }

        template<class OtherType>
        bool operator == (const MyForwardIterator<OtherType> &other) const
        {
            return iterator == other.iterator;
        }

        template<class OtherType>
        bool operator != (const MyForwardIterator<OtherType> &other) const
        {
            return iterator != other.iterator;
        }

        T & operator* () const
        {
            assert(iterator != nullptr && "Invalid iterator dereference");
            return iterator->data;
        }

        T & operator-> () const
        {
            assert(iterator != nullptr && "Invalid iterator dereference");
            return *iterator;
        }

        // One way conversion: iterator -> const_iterator
        operator MyForwardIterator<const T>() const
        {
            return MyForwardIterator<const T>(iterator);
        }
};