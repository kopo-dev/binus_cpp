#include <iostream>

#include "LinkedList.hpp"
#include "LinkedList.cpp"

#include "Queue.hpp"
#include "Queue.cpp"

#include "Stack.hpp"
#include "Stack.cpp"

int main()
{
    std::cout << "Testing DSA-Week3" << std::endl;

    std::cout << "==============================" << std::endl;
    std::cout << "========= Linked List ========" << std::endl;
    std::cout << "==============================" << std::endl << std::endl;

    LinkedList<const char *> list("J'aime");
    list.remove(0);
    list.remove(2);
    list.remove(20);
    list.print();
    std::cout << "List size: " << list.size() << std::endl;
    list.insert("J'aime");
    list.insert("les");
    list.insert("chips");
    list.insert("au");
    list.insert("fromage");
    std::cout << "List size: " << list.size() << std::endl;

    std::cout << "Testing iterator" << std::endl;
    for (MyForwardIterator<const char *> it = list.begin(); it != list.end(); it++)
    {
        std::cout << *it << std::endl;
    }
    std::cout << list.get(1) << std::endl;
    std::cout << list.size() << std::endl;
    list.print();

    std::cout << std::endl << "==============================" << std::endl;
    std::cout << "============ Queue ===========" << std::endl;
    std::cout << "==============================" << std::endl << std::endl;

    Queue<int> queue(0);
    queue.push(1);
    queue.push(2);
    queue.push(3);
    queue.push(4);
    queue.print();
    std::cout << "Popped element: " << queue.pop() << std::endl;
    queue.print();

    std::cout << std::endl <<  "==============================" << std::endl;
    std::cout << "============ Stack ===========" << std::endl;
    std::cout << "==============================" << std::endl << std::endl;

    Stack<int> stack(0);
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    stack.print();
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
    // assert catch segmentation fault
    std::cout << "Popped element: " << stack.pop() << std::endl;
    stack.print();
}