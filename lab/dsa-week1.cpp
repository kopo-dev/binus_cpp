#include <iostream>

void rhombus(int levels) {
    std::cout << "Rhombus" << std::endl;
    
    //top
    std::string leftPoints = "";
    std::string rightPoints = "*";
    std::string spaces = "";

    for (int i = 0; i < levels; i++)
    {
        spaces += " ";
    }
    //first top point
    std::cout << spaces << rightPoints << leftPoints<< std::endl;
    
    for (int i = 0; i < levels; i++)
    {
        leftPoints += "*";
        spaces.pop_back();
        rightPoints += "*";
        std::cout << spaces << rightPoints << leftPoints<< std::endl;
    }
    //bottom
    leftPoints = "";
    rightPoints = "*";
    spaces = " ";
    for (int i = 1; i < levels; i++)
    {
        rightPoints += "*";
        leftPoints += "*";
    }

    for (int i = 0; i < levels; i++)
    {
        std::cout << spaces << rightPoints << leftPoints << std::endl;
        rightPoints.pop_back();
        leftPoints.pop_back();
        spaces += " ";
    }
}

void flippedEquilateralTriangles(int levels)
{
    std::cout << "Flipped Equilateral Triangles" << std::endl;
    std::string leftPoints = "";
    std::string rightPoints = "*";
    std::string spaces = "";

    //right
    for (int i = 0; i < levels; i++)
    {
        rightPoints += "*";
        leftPoints += "*";
    }

    for (int i = 0; i < levels; i++)
    {
        std::cout << spaces << rightPoints << leftPoints << std::endl;
        rightPoints.pop_back();
        leftPoints.pop_back();
        spaces += " ";
    }
    std::cout << spaces << "*" << std::endl;
}


void equilateralTriangles(int levels)
{
    std::cout << "Equilateral Triangles" << std::endl;
    std::string leftPoints = "";
    std::string rightPoints = "*";
    std::string spaces = "";

    for (int i = 0; i < levels; i++)
    {
        spaces += " ";
    }
    //first top point
    std::cout << spaces << rightPoints << leftPoints<< std::endl;
    
    for (int i = 0; i < levels; i++)
    {
        leftPoints += "*";
        spaces.pop_back();
        rightPoints += "*";
        std::cout << spaces << rightPoints << leftPoints<< std::endl;
    }
}

void failedEquilateralTriangles(int levels)
{
    std::cout << "Failed Equilateral Triangles" << std::endl;
    std::string points = "";
    std::string spaces = "";

    for (int i = 0; i < levels; i++)
    {
        points += "*";
        for (int j = 0; j < (levels - i / 2); j++)
        {
            spaces += " ";
        }
        std::cout << spaces << points << std::endl;
        spaces = "";
    }
}

void flippedRightAngledTriangles(int levels)
{
    std::cout << "Flipped Right Angled Triangles" << std::endl;
    std::string display = "";
    std::string spaces = "";

    for (int i = 0; i < levels; i++)
    {
        display += "*";
    }

    for (int i = 0; i < levels; i++)
    {
        spaces += " ";
        std::cout << spaces << display << std::endl;
        display.pop_back();
    }
}

void rightAngledTriangles(int levels)
{
    std::cout << "Right Angled Triangles" << std::endl;
    std::string display = "";
    std::string spaces = "";

    std::cout << "How many level ?" << std::endl;

    for (int i = 0; i < levels; i++)
    {
        spaces += " ";
    }

    for (int i = 0; i < levels; i++)
    {
        spaces.pop_back();
        display += "*";
        std::cout << spaces << display << std::endl;
    }
}

void flippedLeftAngledTriangles(int levels)
{
    std::cout << "Flipped Left Angled Triangles" << std::endl;
    std::string display = "";

    for (int i = 0; i < levels; i++)
    {
        display += "*";
    }

    for (int i = 0; i < levels; i++)
    {
        std::cout << display << std::endl;
        display.pop_back();
    }
}

void leftAngledTriangles(int levels)
{
    std::cout << "Left Angled Triangles" << std::endl;
    std::string display = "";

    for (int i = 0; i < levels; i++)
    {
        display += "*";
        std::cout << display << std::endl;
    }
}

int main()
{
    int levels;

    std::cout << "How many level ?" << std::endl;
    std::cin >> levels;

    leftAngledTriangles(levels);
    flippedLeftAngledTriangles(levels);
    rightAngledTriangles(levels);
    flippedRightAngledTriangles(levels);
    equilateralTriangles(levels);
    flippedEquilateralTriangles(levels);
    failedEquilateralTriangles(levels);
    rhombus(levels);
    return 0;
}