#include "../include/Converter.hpp"
#include <algorithm>

using namespace std;

bool isDigits(const string &str)
{
	return all_of(str.begin(), str.end(), ::isdigit);
}

int value(const char c)
{
	switch (c)
	{
	case 'I':
		return 1;
	case 'V':
		return 5;
	case 'X':
		return 10;
	case 'L':
		return 50;
	case 'C':
		return 100;
	case 'D':
		return 500;
	case 'M':
		return 1000;
	default:
		return 0;
	}
}

void removeBadChar(string &str)
{
	int i = str.length();
	while (i > -1)
	{
		if (value(str[i]) == 0)
			str.erase(i, 1);
		i--;
	}
}

//Decimal to Roman
void Converter::convert(int input)
{
	string result = "Result: ";
	int i = 0;
	int value[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	char *symbol[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

	if (input < 0)
		input *= -1;
	while (input)
	{
		cout << input << " " << value[i] << " " << input / value[i] << endl;
		while (input / value[i])
		{
			cout << symbol[i] << endl;
			input -= value[i];
		}
		i++;
	}
	cout << endl;
}

//Roman to Decimal
void Converter::convert(string input)
{
	int i = input.length() - 1;
	int previous = 0;
	int current = 0;
	int result = 0;

	while (i > -1)
	{
		current = value(input[i]);
		if (current < previous)
			result -= current;
		else
			result += current;
		previous = current;
		i--;
	}
	cout << "Input: " << input << "\nResult: " << result << endl;
}

Converter::Converter()
{
	while (input == "")
	{
		cout << "Type the number to convert" << endl
			 << "--> ";
		cin >> input;
	}
	if (isDigits(input))
	{
		cout << "Decimal to Roman\nInput: " << input << endl;
		convert(stoi(input));
	}
	else
	{
		cout << "Roman to Decimal\nInput: " << input << endl;
		cout << "This converter will ignore any wrong character" << endl;
		removeBadChar(input);
		cout << "Cleaned input: " << input << endl;
		convert(input);
	}
}