#pragma once
#include <iostream>

class Converter
{
private:
    std::string input;
    void convert(std::string);
    void convert(int);

public:
    Converter();
};